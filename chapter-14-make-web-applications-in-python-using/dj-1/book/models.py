from django.db import models
from django.core.urlresolvers import reverse
class Book(models.Model):
    name = models.CharField(max_length=150)
    author = models.CharField(max_length=150)
    price = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    image = models.CharField(max_length=100)

    def __str__(self):
        return  self.name

    def get_absolute_url(self):
        return  reverse('books:detail', kwargs={'pk':self.pk})