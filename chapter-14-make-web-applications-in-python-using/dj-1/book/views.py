from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import  generic
from .models import Book

class IndexView(generic.ListView):
    template_name = 'book/index.html'

    def get_queryset(self):
        return Book.objects.all()

class BookDetailView(generic.DetailView):
    model = Book
    template_name = 'book/detail.html'


class CreateView(generic.CreateView):
    model = Book
    fields = ['name', 'author', 'price', 'image']
    template_name = 'book/creat_book.html'
