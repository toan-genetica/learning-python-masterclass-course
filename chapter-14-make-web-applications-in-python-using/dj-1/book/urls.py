from django.conf.urls import url

from .views import IndexView, BookDetailView, CreateView
app_name = 'books'
urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$',  BookDetailView.as_view(), name='detail'),
    url(r'books/create/$', CreateView.as_view(), name='create'),
    
]