# Chapter 14: Make Web Applications In Python Using Django

## Overview
Python
- Installing Django on Windows and on MAC
- Creating our first Django project
- Creating our own app
    - Overview
    - Creating our own views
    - Applying migrations
    - Adding data to the database tables
    - Connecting to the Database
    - Creating templates
    - Rendering templates
    - Raising a 404 error
    - Designing The detail View
    - Removing the hardcoded urls
    - Namespaces in Django
    - Creating our navigation bar
    - Using static files in Django
    - Design touchup
    - Form to add books
## Manifest File

### Installing Django on Windows and on MAC

### Creating our first Django project
- Creating a virtualenv 
```
# python3 -m venv env
```
- Activating a virtualenv
```
# source env/bin/activate
```
- Creating a Django project
```
(env)# pip install django
(env)# django-admin.py startproject name_project
```

### Creating our own app
- Creating an app in Django project
```
(env)# cd name_project
(env)# python3 manage.py startapp name_app
```

### Overview of an app in Django

### Creating our own views in django

#### Creating url for app

##### How Django processes a request

- When a user requests a page from your Django-powered site, this is the algorithm the system follows to determine which Python code to execute:
    1. Django determines the root `URLconf` module to use. Ordinarily, this is the value of the `ROOT_URLCONF` setting.
    2. Django loads that Python module and looks for the variable `urlpatterns.
    3. Django runs through each `URL pattern`, in order, and stops at the `first` one that matches the `requested URL`
    4. Once one of the `URL patterns` matches, Django imports and calls the given `view`, which is a Python `function` (or a `class-based view`). The view gets passed the following arguments:
        - An instance of `HttpRequest`.
        - If the matched `URL pattern` contained no named groups, then the matches from the regular expression are provided as positional arguments
    5. If no `URL pattern matches`, or if an exception is raised during any point in this process, Django invokes an appropriate `error-handling` view.

- Example:
```
#(project level)
#root_project/urls.py

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^books/', include('book.urls')),
]
```
```
#(app level)
#app/urls.py     

from django.conf.urls import url

from .views import index

urlpatterns = [
    url(r'^$', index, name='index'),
]
```
```
from django.conf.urls import url

from .views import index

urlpatterns = [
    url(r'^$', index, name='index'),
]
```

#### Request and response objects
- Django uses `request` and `response` objects to pass state through the system.
    - When a page is requested, Django creates an `HttpRequest` object that contains `metadata` about the `request`
    -  Then Django loads the appropriate `view`, passing the `HttpRequest` as the first argument to the `view` function. Each `view` is responsible for returning an`HttpResponse` object.

```
from django.http import  HttpResponse

def index(request):
    return  HttpResponse("<h1>This is home page</h1>")
```
- First, we import the class `HttpResponse` from the `django.http` module.
- Next, we define a function called `index`. This is the `view` function. Each `view` function takes an `HttpRequest` object as its first parameter, which is typically named request.
- The view returns an `HttpResponse` object that contains the `generated response`. Each view function is responsible for returning an `HttpResponse` object.

### Applying migrations
- `migrate` which is responsible for applying and unapplying migrations.
```
# python manage.py migrate
```

### Creating books table in Django

#### Models in Django
A `model` is the single, definitive source of information about your data. It contains the essential `fields` and `behaviors` of the data you’re storing. Generally, each model maps to a single database table.
- Each `model` is a Python class that subclasses `django.db.models.Model`.
- Each `attribute` of the model represents a `database field`.
- With all of this, Django gives you an automatically-generated database-access API
```
class Book(models.Model):
    name = models.CharField(max_length=150)
    author = models.CharField(max_length=150)
    price = models.CharField(max_length=100)
    type = models.CharField(max_length=100)

```
#### Migrations
```
# python manage.py makemigrations book
# python manage.py sqlmigrate book 001
# python manage.py migrate
```
### Adding data to the database tables
```
# python manage.py shell

>>> from book.models import Book
>>> obj = Book()
>>> obj.name = 'Book1'
>>> obj.author = 'Tony'
>>> obj.price = '440'
>>> obj.type = 'Mangan'
>>> obj.save()
```

### Filtering the results.
- Filtering all books in database
```
Book.objects.all()
```

- Filtering books by `id`
```
Book.objects.filter(id=1)
```

### Admin pannel in Django
- First, we will register `Book` model that we created with admin panel.
```
#book/admin.py
from django.contrib import admin

from .models import  Book


admin.site.register(Book)

```

- Next, we will create a super admin account to log in admin panel.
```
# python manage.py createsuperuser
```
- Then, we will access `https://127.0.0.1:8000/admin`  and log in with a super admin account created.

### Creating another view in Django
- Add the line code to `book/urls.py`
```
#book/urls.py
...
url(r'^(?P<book_id>[0-9]+)/$',  detail, name='detail'),
...
```
- Add code in `book/views.py`
```
def detail(request, book_id):
    return  HttpResponse("<h1>Detail for book ID:"+ str(book_id)+ "</h1>")
```
We will have a `detail()` function. This function will show informations about a book.

### Connecting to the Database
- Modify `index()` function to get data.
- Import `Book` from `book.models` to get data from `Book` table.
```
from .models import Book
def index(request):
    object_list = Book.objects.all()
    html = ''
    for obj in object_list:
        url = '/books/' + str(obj.id) + '/'
        html += '<a href="' + url + '" >' + str(obj.name) + '</a><br>'
    return  HttpResponse(html)
```

### Creating templates

### Rendering templates
- Combines a given template with a given context dictionary and returns an `HttpResponse` object with that rendered text.
```
from django.shortcuts import render

def index(request):
    object_list = Book.objects.all()
    context = {
        'object_list': object_list
    }
    return  render(request, 'book/index.html', context)
```

### Raising a 404 error
- For convenience, and because it’s a good idea to have a consistent `404` error page across your site, Django provides an `Http404` exception. If you raise `Http404` at any point in a view function, Django will catch it and return the standard error page for your application, along with an `HTTP error code 404`.
```
from django.http import  Http404
```

### Designing the detail view
### Removing the hardcoded urls
- Replace:
```
<a href="/books/{{ book.id }}">{{ book.name }}</a>
```
- By:
```
<a href="{% url 'detail' book.id %}">{{ book.name }}</a>
```

### Namespaces in Django
- URL `namespaces` allow you to uniquely reverse named `URL patterns` even if different applications use the same `URL names`.

### Creating our navigation bar
- Using navbar `Bootstrap 4`

### Using static files in Django
- Websites generally need to serve additional files such as `images`, `JavaScript`, or `CSS`. In Django, we refer to these files as `static files`. Django provides `django.contrib.staticfiles` to help you manage them.

### Generic views in Django
- The two following generic `class-based views` are designed to display data. On many projects they are typically the most commonly used views.
    - `ListView`
    - `DetailView`

### Design touchup

### Form to add books