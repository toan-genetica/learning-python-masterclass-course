# Chapter 01: Introduction

## Course Introduction
- **Instructor:** [Ashutosh Pawar](https://www.udemy.com/user/a9ff8aeb-0700-4b60-950d-ffdce7bf69bc/)

### Course structure
We will learn 6 topics:
- Python Programming Language
- Python GUI Programming With Tkinter
- Django (web framework)
- Flash (web framework)
- Data Analysis
- Web Scraping