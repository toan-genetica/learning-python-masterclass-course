# Chapter 04: Functions & Modules In Python

## Overview
Python - How to use Functions and Modules

## Manifest File

### Functions:
You can pass data, known as parameters, into a function.
A function can return data as a result.

###### Passing arguments to functions in Python
><pre>def add(x, y):
>    print(x + y)
>
>add(20, 30)</pre>

###### Making function return value in Python
><pre>def add(x, y):
>    z = x + y
>    return z
>
>result = add(2, 3)
>print(result)</pre>

###### Passing functions as arguments in Python
><pre>def add(x, y):
>    return x + y
>
>def square(z):
>   return z * z
>
>result = square(add(2, 6))
>print(result)</pre>

###### Default Parameter Value
The following example shows how to use a default parameter value.
If we call the function without argument, it uses the default value:

><pre>def my_function(country = "VietNam"):
>   print("I am from " + country)
>
>my_function()
>my_function("USA")</pre>

###### Parameters or Arguments?
The terms parameter and argument can be used for the same thing: information that are passed into a function.
>From a function's perspective:
>- A parameter is the variable listed inside the parentheses in the function definition.
>- An argument is the value that is sent to the function when it is called.

### Modules
Consider a module to be the same as a code library.
A file containing a set of functions you want to include in your application.