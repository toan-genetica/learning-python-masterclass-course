# Chapter 12: Building Database Apps With PostgreSQL & Python

## Overview
Python
- Introduction to database
- PostgreSQL
- Installing PostgreSQL
- Creat a database
- Creating Table & Adding Data
- Reading Data From Database
- Setting Up Virtualenv, installing Psycopg2
- Creating database tables with python
- Adding data to database tables with python


## Manifest File

### Instroduction to database

#### Why store data?
Once you store the data, you can perform the following operations:
- Read: data is stored into the database you can go ahead and read the data as and when you want. So let's say for example you're building a banking application.
- Update/ modify: The next biggest advantage of actually saving data and database is you can update and modify data.
- Delete: you can go ahead and simply erase that data delete that data from the database.

So these following operations which you can perform on the database makes it worthy to actually store in a database

#### How data is stored in a database
- Data in a database is stored in form of tables

#### What we will learn
- What is PostgreSQL.
- How to download and install PostgreSQL on Windows and Mac.
- How to create database and database tables
- How to connect Python code with database to save, read, update and delete data from database

### PostgreSQL
- PostgreSQL is a general purpose and object-realational database management system.
- Object-relational: Objects, classses & inheritance is directly supported in database schemas.
- it now works on MAC, Windows and as well Unix like platforms

### Installing PostgreSQL

### Creat a database

###### List of database
><pre># \l</pre>

###### Create a database
><pre># CREATE DATABASE input_name;</pre>

###### Connect to a database
><pre># \c input_name</pre>

###### Delete a database
><pre># drop database input_name;</pre>

### Creating Table & Adding Data

###### Create a table
><pre># CREATE TABLE staff(name text,age int,number int);</pre>

###### List of relations
><pre># \d</pre>

###### Adding Data
><pre># INSERT INTO staff (name,age,number) VALUES ('Toan',25,99999);</pre>

### Reading Data From Database

###### Retrieve all field data from table
><pre># SELECT * FROM staff;</pre>

###### Retrieve data conditionally
><pre># SELECT * FROM staff WHERE age=25;</pre>

###### Retrive a field data
><pre># SELECT name FROM staff WHERE age=25;</pre>

### Setting Up Virtualenv, installing Psycopg2

### Connecting to databases with python
><pre>import psycopg2
>
>con = psycopg2.connect(
>    dbname='postgres',
>    user='postgres',
>    password='test@123',
>    host='localhost',
>    port='5432'
>)</pre>
Connect successful when no errors will appear.

### Creating database tables with python
><pre>import psycopg2
>
>con = psycopg2.connect(
>   dbname='postgres',
>   user='postgres',
>   password='test@123',
>   host='localhost',
>   port='5432'
>   )
>cur = con.cursor()
>cur.execute('''CREATE TABLE staff(ID SERIAL, NAME TEXT, AGE INT, NUMBER INT);''')
>print('table is created successfully')
>con.commit()
>con.close()</pre>

### Adding data to database tables with python
><pre>import psycopg2
>
>con = psycopg2.connect(
>    dbname='postgres',
>    user='postgres',
>    password='test@123',
>    host='localhost',
>    port='5432'
>    )
>cur = con.cursor()
>cur.execute('''INSERT INTO staff (NAME,AGE,NUMBER) VALUES ('TOAN',25,65656);''')
>print('data is updated successfully')
>con.commit()
>con.close()</pre>

### Submitting Data To Database
><pre>def add_data():
>    con = psycopg2.connect(
>    dbname='vtoanpkdb',
>    user='postgres',
>    password='test@123',
>    host='localhost',
>    port='5432'
>)
>    cur = con.cursor()
>    # insert data to table
>    name = input('Enter name: ')
>    age = input('Enter age: ')
>    query = '''INSERT INTO student1 (NAME,AGE) VALUES (%s,%s);'''
>    cur.execute(query,(name, age))
>    print('data is added successfully')
>    con.commit()
>    con.close()
>
>add_data()</pre>

### Build a app with Tkinter

##### GUI app
![](app.png)

##### Reference code
[App Code](chapter-12-building-database-apps-with-postgreSQL-and-python/app.py)
