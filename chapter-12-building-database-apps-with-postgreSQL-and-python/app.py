from tkinter import *
import psycopg2


root = Tk()

# Add data to table
def get_data(name, age):
    con = psycopg2.connect(
        dbname='vtoanpkdb',
        user='postgres',
        password='test@123',
        host='localhost',
        port='5432'
    )
    cur = con.cursor()
    query = 'INSERT INTO student1 (name,age) VALUES (%s, %s);'
    values = (name, age)
    cur.execute(query, values)
    print('Data inserted')
    con.commit()
    con.close()

    #call func list all data from table
    display_all()

# Function search data in table
def get_search(id):
    con = psycopg2.connect(
        dbname='vtoanpkdb',
        user='postgres',
        password='test@123',
        host='localhost',
        port='5432'
    )
    cur = con.cursor()
    query = 'SELECT * FROM student1 WHERE id=%s;'
    cur.execute(query, (id))
    row= cur.fetchone()
    search_display(row)
    con.commit()
    con.close()

def search_display(row):
    listbox = Listbox(frame, width=20, height=1)
    listbox.grid(row=7, column=1)
    listbox.insert(END, row)

# Function display all data from table
def display_all():
    con = psycopg2.connect(
        dbname='vtoanpkdb',
        user='postgres',
        password='test@123',
        host='localhost',
        port='5432'
    )
    cur = con.cursor()
    query = 'SELECT * FROM student1;'
    cur.execute(query)
    row= cur.fetchall()
    display_all_result(row)
    con.commit()
    con.close()

def display_all_result(row):

    listbox = Listbox(frame, width=20, height=5)
    listbox.grid(row=8, column=1)
    for x in row:
        listbox.insert(END, x)

# GUI app
canvas = Canvas(root, height=480, width=900)
canvas.pack()

frame = Frame()
frame.place(relx=0.3, rely=0.1, relwidth=0.8, relheight=0.8)

label = Label(frame, text='Add data')
label.grid(row=0, column=1)

label = Label(frame, text='Name: ')
label.grid(row=1, column=0)

entry_name = Entry(frame)
entry_name.grid(row=1, column=1)

label = Label(frame, text='Age: ')
label.grid(row=2, column=0)

entry_age = Entry(frame)
entry_age.grid(row=2, column=1)

button = Button(frame, text='ADD', command=lambda : get_data(entry_name.get(), entry_age.get()))
button.grid(row=4, column=1)

label = Label(frame, text='Search')
label.grid(row=5, column=1)

label = Label(frame, text='Search by ID')
label.grid(row=6, column=0)

entry_search = Entry(frame)
entry_search.grid(row=6, column=1)

button = Button(frame, text='Search', command=lambda : get_search(entry_search.get()))
button.grid(row=6, column=3)

display_all()
root.mainloop()