# Chapter 07: Functional Programming In Python

## Overview
Python
- Functional programming
- Lambda
- Map

## Manifest File

### Functional programming:
You can pass data, known as parameters, into a function.
A function can return data as a result.

#### Passing arguments to functions in Python
><pre>def add(x):
>   return x + 10
>
>def twice(func, arg):
>    return func(func(arg))
>
>result = twice(add, 10)
>print(result)

### Lambda
A lambda function is a small anonymous function.
A lambda function can take any number of arguments, but can only have one expression.
><pre>result = lambda a, b, c : a + b + c
>print(result(5, 6, 2))</pre>

>or

><pre>result = (lambda a, b, c : a + b + c)(5, 6, 2)
>print(result)</pre>

##### Why Use Lambda Functions?
The power of lambda is better shown when you use them as an anonymous function inside another function.
Say you have a function definition that takes one argument, and that argument will be multiplied with an unknown number:
><pre>def fuc(n):
>    return lambda x : x * n
>
>result = fuc(2)
>print(result(3))

>or

><pre>def fuc(n):
>    return (lambda x : x * n)(3)
>
>result = fuc(2)
>print(result)


### Map
map() function returns a map object(which is an iterator) of the results after applying the given function to each item of a given iterable (list, tuple etc.)
><pre>Syntax :
>   map(fun, iter)
>Parameters :
>   fun : It is a function to which map passes each element of given iterable.
>   iter : It is a iterable which is to be mapped.</pre>

### Filters

### Generators
Generators are used to create iterators, but with a different approach. Generators are simple functions which return an iterable set of items, one at a time, in a special way.
><pre>def gen():
>    count = 0
>    while count < 5:
>        yield count
>        count += 1
>
>for i in gen():
>    print(i)</pre>

