# COURSE: The Complete Python Masterclass: Learn Python From Scratch

Learning Course: The Complete Python Masterclass: Learn Python From Scratch https://www.udemy.com/course/python-masterclass-course/

## BRIEF PLANNING: 

- Expected Time: 96 hours

- Finish day: 29/07/2020

## Day 1 (16/07/2020)
### Today's progress
Got started with introduction about this course in Chapter 1
Learned basic python concepts in Chapter 2 and control structures in Chapter 3

### Thoughts
Quite relaxing when reviewing the basic python

### Links to work
- [Chapter 1](chapter-01-introduction/README.md)
- [Chapter 2](chapter-02-basic-python-concepts/README.md)
- [Chapter 3](chapter-03-control-structures-in-python/README.md)

## Day 2 (17/07/2020)
### Today's progress
Learned
- Functions & Modules in python in Chapter 4 
- Exception handling & File handling in Chapter 5
- Some more types in Chapter 6
- Functional programming in Chapter 7
- Object oriented programming in Chapter 8

### Thoughts
Preparing to learn more difficult lessons

### Links to work
- [Chapter 4](chapter-04-functions-and-modules-in-python/README.md)
- [Chapter 5](chapter-05-exception-handlingand-file-handling-in-python/README.md)
- [Chapter 6](chapter-06-some-more-types-in-python/README.md)
- [Chapter 7](chapter-07-functional-programming-in-python/README.md)
- [Chapter 8](chapter-08-object-oriented-programming-in-python/README.md)

## Day 3 (18/07/2020)
### Today's progress
Learned
- Regular expressions in Chapter 9 
- Create GUI Apps In Python Using Tkinter in Chapter 10

### Thoughts
Tkinter module is great

### Links to work
- [Chapter 9](chapter-09-regular-expressions-in-python/README.md)
- [Chapter 10](chapter-10-create-gui-apps-in-python-using-tkinter/README.md)

## Day 4 (19/07/2020)
### Today's progress
Learned
- Using drop downs, toolbar, status bar, message box, drawing tkinter in Chapter 10 
- Building a caculator app in Chapter 11
- Building database apps with postgreSQL and python in Chapter 12

### Thoughts
Interested in building an application

### Links to work
- [Chapter 11](chapter-11-building-calculator-app-using-tkinter/README.md)
- [Chapter 12](chapter-12-building-database-apps-with-postgreSQL-and-python/README.md)

## Day 5 (20/07/2020)
### Today's progress
- Continued to learn in Chapter 12
    - Adding Data To Database Tables With Python
    - Submitting Data To Database
    - Build a app with Tkinter to connect to Postgres
- Learned in Chapter 13
    - Pandas, Series and DataFrame Module

### Thoughts
Interested in data analysis

### Links to work
- [Chapter 12](chapter-12-building-database-apps-with-postgreSQL-and-python/README.md)
- [Chapter 13](chapter-13-data-analysis-using-python/README.md)

## Day 6 (21/07/2020)
### Today's progress
- Continued to learn in Chapter 13
    - Pandas and Series
        - Converting dictionaries to series
        - Introduction to data frames
        - Changing column sequence
        - Changing columns & transposing dataframe
        - Reindexing series & dataframes
        - Deleting rows & coulumns
        - Arithmetic operations on dataframe and series
        - Arithmetic operations in between dataframe and series
        - Sorting series and dataframes
        - Sorting according to values
        - Handling duplicate values
        - Calculating sum, max & min values
        - Dropping nan values
        - Loading data from a file
    - Numpy and Matplotlib
        - Creating numpy array
        - Logspace & Linspace
        - Slicing a numpy array
        - Advanced indexing and slicing techniques
        - Broadcasting
        - Iterating using nditer
        - Plotting data using Matplotlib
        - Analysing supermarket sales data

### Thoughts        
Interested in data analysis

### Links to work
- [Chapter 13](chapter-13-data-analysis-using-python/README.md)

## Day 7 (22/07/2020)
### Today's progress
- Continued to learn in Chapter 13
    - Analysing supermarket sales data
- Made web applications in python using Django in Chapter 14
    - Installing Django on Windows and on MAC
    - Creating our first Django project
    - Creating our own app
    - Overview of an app in Django

### Thoughts        
Interested in Django

### Links to work
- [Chapter 13](chapter-13-data-analysis-using-python/README.md)
- [Chapter 14](chapter-14-make-web-applications-in-python-using/README.md)

## Day 8 (23/07/2020)
### Today's progress
- Continued to learn in Chapter 14
   - Creating our own views
    - Applying migrations
    - Adding data to the database tables
    - Connecting to the Database
    - Creating templates
    - Rendering templates
    - Raising a 404 error
    - Designing The detail View
    - Removing the hardcoded urls
    - Namespaces in Django
    - Creating our navigation bar
    - Using static files in Django
    - Design touchup
    - Form to add books

### Thoughts        
We should focus on:
- Models, Migrations.
- HttpRequest, HttpResponse, Http404, Render.
- Views, Forms.

### Links to work
- [Chapter 14](chapter-14-make-web-applications-in-python-using/README.md)

## Day 9 (24/07/2020)
### Today's progress
- Django 2.0
    - Creating a project
    - Creating an app
    - Overview of an app
    - Creating a view
    - Migrations
    - Creating a model
    - Creating and Adding database tables
    - Filtering the results
    - Creating a super user
    - Creating another view
    - Connecting to the database
    - Creating templates
    - Using render
    - Raising 404 Error
    - Designing the detail view
    - Removing hard coded URLs
    - Using Namespaces
    - Designing Navbar
    - Navbar Touchup

### Thoughts        
We should focus on:
- Models, Forms, Views and Urls

### Links to work

- [Chapter 15](chapter-15-make-web-applications-with-python-using-django-2/README.md)

## Day 10 (25/07/2020)

### Today's progress

- Introduction API
- Building a basic REST API using Django REST Framework
    - Models
    - Serializers
    - Viewsets
    -  Creating API Endpoints

### Thoughts

We should focus on:
- Models, Serializers, Viewsets, Routers.

### Links to work

- [Chapter 17](chapter-17-building-REST-API's-with-python-and-django/README.md)

## Day 11 (26/07/2020)

### Today's progress

- Building a basic REST API using Django REST Framework
    - Adding Image Field
    - Filtering
    - Search functionality
    - API Authentication

### Thoughts

We should focus on:
- Serializers, Filters, Authentication.

### Links to work

- [Chapter 17](chapter-17-building-REST-API's-with-python-and-django/README.md)

## Day 12 (27/07/2020)

### Today's progress

- Python Web Crawler chapter 18
- Complex Coding Challenges chapter 19

### Thoughts

We should focus on:
- Web Srawler is quite difficult to understand

### Links to work

- [Chapter 18](chapter-18-learn-how-to-crawl-websites-using-python-web-crawling/README.md)
- [Chapter 19](chapter-19-complex-coding-challenges/README.md)

## Day 13 (28/07/2020)

### Today's progress

- Complex Coding Challenges

### Thoughts

### Links to work

- [Chapter 19](chapter-19-complex-coding-challenges/README.md)

## Day 14 (29/07/2020)

### Today's progress

- Complex Coding Challenges

### Thoughts

- Challenge 1
- Challenge 2
- Challenge 4
- Challenge 7

### Links to work

- [Chapter 19](chapter-19-complex-coding-challenges/README.md)

## Day 15 (3/08/2020)

### Today's progress
- Automation With Selenium Web Driver and Python
    - Introduction
    - Install selenium
    - Opening a URL
    - Find elements by XPATH
    - Clicking lnks
    - Refreshing a webpage
    - Using forward and backward navigation buttons
    - Scrolling and getting the current url


### Thoughts
- Focus on:
    - Selenium
    - Webdriver

### Links to work

- [Chapter 20](chapter-20-automation-with-selenium-web-driver-and-python/README.md)

## Day 16 (5/08/2020)

### Today's progress
- Chapter 21 Building a facebook auto poster
- Chapter 22 Python best practices writing clean and efficient python code
- Chapter 23 Network programming in python using sockets  building a chat application

### Thoughts

### Links to work

- [Chapter 21](chapter-21-building-a-facebook-auto-poster/README.md)
- [Chapter 22](chapter-22-python-best-practices-writing-clean-and-efficient-python-code/README.md)
- [Chapter 23](chapter-23-network-programming-in-python-using-sockets-building-a-chat-application/README.md)