# Chapter 02: Basic Python Concept

## Overview
Python - Basic Syntax

## Manifest File

### Hello world program in Python:
The print function in Python allows us to print/display some text as the output.
The simple line of code to display hello world is: print('Hello World').

### Mathematical operations in Python.
We can perform mathematical operations like:

| Operator | Name            | Example  | Result|
| ---------|:---------------:| --------:| -----:|
| +        | Addition        | 5 + 5    | 10    |
| -        | Subtraction     | 5 - 5    | 0     |
| *        | Multiplication  | 5 * 5    | 25    |
| /        | Division        | 5 / 5    | 1.0   |
| %        | Modulus         | 5 % 5    | 0     |
| **       | Exponentiation  | 5 ** 5   | 3125  |
| //       | Floor division  | 6 // 5   | 1     |

### Strings in Python
Python treats single quotes the same as double quotes. Creating strings is as simple as assigning a value to a variable.

For example:
- var1 = 'Hello World!'
- var2 = "Python Programming"


### Variables in Python
Variables are containers for storing data values.
Unlike other programming languages, Python has no command for declaring a variable.

Rules for Python variables: 
- A variable name must start with a letter or the underscore character
- A variable name cannot start with a number
- A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )
- Variable names are case-sensitive (age, Age and AGE are three different variables)
