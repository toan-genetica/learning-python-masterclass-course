# Chapter 18: Learn How To Crawl Websites Using Python Web Crawling

## Overview

- Python Web Crawler
    - Introduction
    - Code reference


## Manifest File

### Introduction

- Web scraping, often called web crawling or web spidering, or “programmatically going over a collection of web pages and extracting data,” is a powerful tool for working with data on the web.
- With a web scraper, you can mine data about a set of products, get a large corpus of text or quantitative data to play around with, get data from a site without an official API, or just satisfy your own personal curiosity.

### [Code reference](web_crawling)