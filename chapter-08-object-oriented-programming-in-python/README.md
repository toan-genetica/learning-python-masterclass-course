# Chapter 08: Object Oriented Programming In Python

## Overview
Python
- Introduction
- Class and Class Attributes
- Instance Attributes and Contructor
- Methods
- Inheritance
- Recursion
- Sets
- Itertools

## Manifest File

### Introduction
Python is an object oriented programming language.
Almost everything in Python is an object, with its properties and methods.
A Class is like an object constructor, or a "blueprint" for creating objects.
### Class and Class Attributes

#### Class
A Class is like an object constructor, or a "blueprint" for creating objects.

#### Class Attribute
A class attribute is a Python Variable that belongs to a class rather than a particular object. This is shared between all other objects of the same class and is defined outside the constructor function __init__(self,…), of the class.
><pre>class MyClass:        #define a class 
>    x = 5              #define a class attribute</pre>
### Instance attributes and contructor

#### Instance attributes
An instance attribute is a Python variable belonging to only one object. It is only accessible in the scope of the object and it is defined inside the constructor function of a class. For example,' __init__'(self,..).

#### Contructor
The __init__ method is similar to constructors in C++ and Java. It is run as soon as an object of a class is instantiated. The method is useful to do any initialization you want to do with your object.
###### A Sample class with init method:
><pre>class Person: 
>  
>    # init method or constructor  
>    def __init__(self, name): 
>        self.name = name </pre>
### Methods
A method in python is somewhat similar to a function, except it is associated with object/classes. Methods in python are very similar to functions except for two major differences.
The method is implicitly used for an object for which it is called.
The method is accessible to data that is contained within the class.
><pre>class Pet(object):
>   def my_method(self):
>      print("I am a Cat")
>cat = Pet()
>cat.my_method()</pre>
### Inheritance
Inheritance allows us to define a class that inherits all the methods and properties from another class.
Parent class is the class being inherited from, also called base class.
Child class is the class that inherits from another class, also called derived class.

### Recursion
Python also accepts function recursion, which means a defined function can call itself.
Recursion is a common mathematical and programming concept. It means that a function calls itself. This has the benefit of meaning that you can loop through data to reach a result.
The developer should be very careful with recursion as it can be quite easy to slip into writing a function which never terminates, or one that uses excess amounts of memory or processor power. However, when written correctly recursion can be a very efficient and mathematically-elegant approach to programming.
><pre>def recur(x):
>    if x == 1:
>        return 1
>    return x * (recur(x-1))
>result = recur(5)
>print(result)

><pre>120</pre>

### Sets
A set is a collection which is unordered and unindexed. In Python sets are written with curly brackets.
Sets are unordered, so you cannot be sure in which order the items will appear.
### Itertools
### Overloading
### Data Hinding