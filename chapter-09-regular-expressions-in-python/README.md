# Chapter 09: Regular Expressions In Python

## Overview
Python
- Regular expressions in Python, Search & find all


## Manifest File

### Regular expressions in Python, Search & find all
><pre>import re
>
>st = r'hai'</pre>

Regular expressions:
><pre>if re.match(st, 'sdfsdhaidmskfmsdkmfkds'):
>    print('Match')
>else:
>    print('Not Match')

>Not Match

Search expressions:
><pre>if re.search(st, 'shfkjshfkjshkfsdhi'):
>    print('Found')
>else:
>    print('Not Found')

>Not Found</pre>

Find all objects:

><pre>print(re.findall(st,'snfsjhaisihai'))</pre>

>['hai', 'hai']

### Find & replace
><pre>import re
>
>
>string = ('I am John, Hello John')
>obj = r'John'
>rep_obj =r'Toan'
>
>new_string = re.sub(obj, rep_obj, string)
>print(new_string)</pre>

>I am Toan, Hello Toan

### The dot metacharacter, Caret & dollar metacharacter
><pre>import re
>
>st = r'^ha.i$'
>if re.match(st, 'hadi'):
>    print('Match')
>else:
>    print('Not Match')

>Match