
# Chapter 10: Create GUI Apps In Python Using Tkinter

## Overview
Python
- Introduction
- Using frames
- Grid layout
- Self adjusting widgets
- Handling button clicks
- Using Classes


## Manifest File

### Introduction
Python has a lot of GUI frameworks, but Tkinter is the only framework that’s built into the Python standard library. Tkinter has several strengths. It’s cross-platform, so the same code works on Windows, macOS, and Linux. Visual elements are rendered using native operating system elements, so applications built with Tkinter look like they belong on the platform where they’re run.

Tkinter is lightweight and relatively painless to use compared to other frameworks. This makes it a compelling choice for building GUI applications in Python, especially for applications where a modern sheen is unnecessary, and the top priority is to build something that’s functional and cross-platform quickly.

### Using frames
The Frame widget is very important for the process of grouping and organizing other widgets in a somehow friendly way. It works like a container, which is responsible for arranging the position of other widgets.

It uses rectangular areas in the screen to organize the layout and to provide padding of these widgets. A frame can also be used as a foundation class to implement complex widgets.

#### Syntax
Here is the simple syntax to create this widget 
>w = Frame ( master, option, ... )


#### Parameters
- master − This represents the parent window.

- options − Here is the list of most commonly used options for this widget. These options can be used as key-value pairs separated by commas

><pre>from tkinter import *
>
>root = Tk()
>
>frame = Frame(root)
>frame.pack()
>
>otherframe = Frame(root)
>otherframe.pack(side=BOTTOM)
>
>redbutton1 = Button(frame, text="Red",fg ='red')
>redbutton1.pack( side = LEFT)
>bluebutton1 = Button(frame, text="Blue",fg ='blue')
>bluebutton1.pack( side = RIGHT)
>bluebutton2 = Button(otherframe, text='Blue', fg='blue')
>bluebutton2.pack(side = LEFT)
>redbutton2 = Button(otherframe, text="Red",fg ='red')
>redbutton2.pack( side = RIGHT)
>
>root.mainloop()</pre>

###### Result
![](tkframe.png)

### Grid layout
Position of items base on Grid Layout

| Grid     | Column 0           | Column 1           | Column 3           |
| ---------|:------------------:|:------------------:|:------------------:|
| row 0    | (row=0, column=0)  | (row=0, column=1)  | (row=0, column=3)  |
| row 1    | (row=1, column=0)  | (row=1, column=1)  | (row=1, column=3)  |
| row 2    | (row=2, column=0)  | (row=2, column=1)  | (row=2, column=3)  |


><pre>from tkinter import *
>
>root = Tk()
>
>label1 = Label(root, text='User name: ')
>label2 = Label(root, text='Last name: ')
>
>entry1 = Entry(root)
>entry2 = Entry(root)
>
>label1.grid(row=0, column=0)
>label2.grid(row=1, column=0)
>entry1.grid(row=0, column=1)
>entry2.grid(row=1, column=1)
>
>root.mainloop()</pre>

###### Result
![gridlayout](gridlayout.png)

### Self adjusting widgets
><pre>from tkinter import *
>
>root = Tk()
>
>label1 = Label(root, text='First', bg='Red', fg='White')
>label1.pack(fill=X)
>
>label2 = Label(root, text='Second', bg='Blue', fg='Yellow')
>label2.pack(side=LEFT,fill=Y)
>
>root.mainloop()</pre>

###### Result
![](adjusting_widgets.png)

### Handling button clicks
><pre>from tkinter import *
>
>root = Tk()
>
>def do_something():
>    print('You clicked this button')
>
>button = Button(root, text='Click me!', command=do_something)
>button.pack()
>
>root.mainloop()</pre>

###### Results
![](button_click.png)
![](result_button_click.png)

### Using classes
We will use our knowlegde that we studied in the previous chapter.
We will use OOP combined with Tkinter to learn classes in Tkinter  and build user interfaces
><pre>from tkinter import *
>class MyButtons:
>
>    def __init__(self, button_root):
>        frame = Frame(button_root)
>        frame.pack()
>
>        self. button1 = Button(frame, text='Click Me!', command=self.printmethod)
>        self.button1.pack()
>        
>        self.button2 = Button(frame, text='Exit', command=frame.quit)
>        self.button2.pack(side=LEFT)
>
>    def printmethod(self):
>        print('You clicked on this button!')
>
>
>root = Tk()
>first_class = MyButtons(root)
>
>root.mainloop()</pre>

###### Results
![](classes.png)
- "Click Me!" program will call 'printmethod' and print 'You clicked on this button!' to screen.
- "Exit" program will quit the frame

### Using drop downs, toolbar, status bar
><pre>from tkinter import *
>
>root = Tk()
>
>def  func_print():
>    print('This a functions')
>
>mymenu = Menu(root)
>root.config(menu=mymenu)
>
>mymenu1 = Menu(mymenu)
>mymenu.add_cascade(label='File', menu=mymenu1)
>
>mymenu1.add_command(label='Projects', command=func_print)
>mymenu1.add_command(label='Save', command=func_print)
>
>mymenu1.add_separator()
>mymenu1.add_command(label='Exit', command=func_print)
>
>mymenu2 = Menu(mymenu)
>mymenu.add_cascade(label='Edit', menu=mymenu2)
>
>
>root.mainloop()</pre>

###### Results
![](drop_down.png)

### Message Box
><pre>from tkinter import *
>import tkinter.messagebox
>
>root = Tk()
>tkinter.messagebox.showinfo('Title', 'This is awesome')
>
>response = tkinter.messagebox.askquestion('Question1', "Do you like coffe?")
>
>if response == 'yes':
>    print('Coffe')
>
>root.mainloop()</pre>
###### Results
![](messagebox.png)
![](messagebox2.png)

### Drawing
><pre>from tkinter import *
>
>root = Tk()
>
>canvas = Canvas(root, width=200, height=100)
>canvas.pack()
>
>newline = canvas.create_line(0, 0, 50, 100)
>newline1 = canvas.create_line(10, 10, 100, 100, fill='Red')
>
>root.mainloop()</pre>
![](drawing.png)
