# Chapter 13: Data Analysis Using Python

## Overview
Python
- Data analysis using python
- Installing tools for data analysis
- Introduction to pandas & series
    - Pandas and Series
        - Converting dictionaries to series
        -   Introduction to data frames
        - Changing column sequence
        - Changing columns & transposing dataframe
        - Reindexing series & dataframes
        - Deleting rows & coulumns
        - Arithmetic operations on dataframe and series
        - Arithmetic operations in between dataframe and series
        - Sorting series and dataframes
        - Sorting according to values
        - Handling duplicate values
        - Calculating sum, max & min values
        - Dropping nan values
        - Loading data from a file
        - Analyzing file data
    - Numpy and Matplotlib
        - Creating numpy array
        - Logspace & Linspace
        - Slicing a numpy array
        - Advanced indexing and slicing techniques
        - Broadcasting
        - Iterating using nditer
        - Plotting data using Matplotlib
- Analysing supermarket sales data

## Manifest File

### Data Analysis Using Python

#### What is data analysis?
- Data analysis is analyzing data from multiple sources to extract meaningful information.

#### Why data analysis?
- But the purpose of a data analysis or the basic significance of data analysis is to help make  better decisions for organizations as well as to make the best use of the available data.
- Best use of available data.

### Why data analysis is the future?
- Raw data can be converted into useful information using data analysis and later that information can be used to make logical conclusions.

### Why python for data analysis?
- Faster as compared to other alternatives
- Has simple syntax
- Has multiple libraries for data analysis

### Installing Tools For Data Analysis

### Introduction to Pandas & Series
- When working with tabular data, such as data stored in spreadsheets or databases, `Pandas` is the right tool for you. `Pandas` will help you to explore, clean and process your data. In `Pandas`, a data table is called a `DataFrame`.

![](dataframe.png)

#### Series
- `Series` is a one-dimensional labeled array capable of holding data of any type (integer, string, float, python objects, etc.). The axis labels are collectively called index.
- A pandas `Series` can be created using the following constructor:
><pre>pandas.Series( data, index, dtype, copy)</pre>

##### The parameters of the constructor are as follows:
![](series.png)

##### A series can be created using various inputs like:
- Array
- Dict
- Scalar value or constant

##### Create an empty series
><pre>import pandas as pd
>s = pd.Series()
>print(s)</pre>

### Converting dictionaries to series
![](series_dictionary.png)

### Introduction to data frames
A `DataFrame` is a two-dimensional data structure, i.e., data is aligned in a tabular fashion in rows and columns.
Features of `DataFrame`
- Potentially columns are of different types
- Size – Mutable
- Labeled axes (rows and columns)
- Can Perform Arithmetic operations on rows and columns

![](ex_frame.png)

You can think of it as an SQL table or a spreadsheet data representation.

### Changing Column Sequence

##### Changing the position of columns
![](changecolumn.png)

##### Adding one more column
![](addcolumn.png)

##### Retrieving a column
![](retrieveacolumn.png)

##### Retrieving a row
![](retrievearow.png)

### Changing columns & transposing dataframe

#### Changing columns

##### Adding data to empty column
`Projects` column has no any infomations and we must add data in it.

![](add_data_to_column.png)

##### Adding a new columns and data in it
We will add another new column and write data into it as well.

![](add_new_column.png)

#### Transposing dataframe
We will change rows with columns to each other and vice versa.
![](change_column_with_row.png)

### Reindexing series & dataframes

#### Reindexing series
![](reindex_series.png)

#### Reindexing dataframes

##### Reindexing row
![](reindex_row_dataframes.png)

##### Reindexing column
![](reindex_column_dataframes.png)

### Deleting rows & coulumns

#### Deleting rows
![](delete_row_dataframes.png)

#### Deleting columns
![](delete_column_dataframes.png)

### Arithmetic operations on dataframe and series

#### Arithmetic operations on series
![](operations_series.png)

#### Arithmetic operations on series
- Create a `DataFrame` is called `frame1`

![](operations_dataframe1.png)

- Create a `DataFrame` is called `frame2`

![](operations_dataframe2.png)

- `frame1` plus `frame2` is a new DataFrame with new values  

![](operations_dataframe3.png)

### Arithmetic operations in between dataframe and series
- Create a `Series`

![](operations_dataframe_with_series1.png)

- Creat a `DataFrames`

![](operations_dataframe_with_series2.png)

- `DataFrame` subtract `Series` is a new DataFrame with new values

![](operations_dataframe_with_series3.png)

### Sorting series and dataframes index
- Use `sort.index` method to sort index with ascending or descending

#### Sorting series
![](sort_series.png)

#### Sorting dataframes

##### Sorting by row
- Add argument `axis = 0` if sorting by row

![](sort_row_dataframe.png)

###### Sorting by column
- Add argument `axis = 1` if sorting by column

![](sort_column_dataframe.png)

### Sorting according to values
- Use `sort_values` method to sort values with ascending or descending

#### Sorting series
![](sort_values_series.png)

#### Sorting dataframes
![](sort_values_dataframes.png)

### Handling duplicate values
![](check_unique_index.png)

### Calculating sum, max & min values
- Use `sum()` method to sum values by column

![](sum_by_column_dataframe.png)

- Use `sum(axis=1)` method to sum values by row

![](sum_by_row_dataframe.png)

-Use `min()` or `max()` method to find a min or max values by column

![](fin_min_max_by_column.png)

-Use `min(axis=1)` or `max(axis=1)` method to find a min or max values by row

![](fin_min_max_by_row.png)

### Dropping `nan` in values
- Use `dropna()` to drop `nan` values

#### Dropping `nan` values series

![](drop_nan_values_series.png)

#### Dropping `nan` values in dataframes

![](drop_nan_values_dataframes.png)

#### Filling into `nan` values
- Use `fillna(values)` to fill into `nan` values

![](fill_nan_values.png)

### Loading data from a file
![](load_data_from_file.png)

### Analyzing file data

### Creating Numpy Array

![](numpy_array.png)

### Another way to create an array

![](numpy_array2.png)

### Logspace & Linspace
#### Linspace
- Linspace gives evenly spaced samples.

![](lin_space.png)

#### Logspace
- LogSpace returns even spaced numbers on a log scale. Logspace has the same parameters as np.linspace.

![](log_space.png)

### Slicing a numpy array

### Advanced indexing and slicing techniques

![](advanced_slicing.png)

### Broadcasting

![](broadcasting.png)

### Iterating using nditer

![](nditer.png)

### Plotting data using Matplotlib

![](matplotlib.png)

### Analysing supermarket sales data

#### Part 1: Reading .csv file

![](analysis_sales_part1.png)

#### Part 2: Switching up the theme

#### Part 3: Accessing different parts of data

![](analysis_sales_part3.png)

#### Part 4: Selecting rows On a condition

![](analysis_sales_part4.png)

#### Part 5: Queries to find conditional data

![](analysis_sales_part5.png)

#### Part 6: Sum, max, min & average

![](analysis_sales_part6.png)

#### Part 7: Using GroupBy to group data by location

![](analysis_sales_part7.png)

#### Part 8: Finding market share

![](analysis_sales_part8.png)

#### Part 9: Classifying shoppers

![](analysis_sales_part9.png)

#### Part 10: Analysing memberships & ratings

![](analysis_sales_part10.png)

#### Part 11: Answering multiple queries

#### Part 12: Classifying sales by day

![](analysis_sales_part12.png)

#### Part 13: Classifying sales by month

![](analysis_sales_part13.png)

#### Part 14: Classifying sales by hour

![](analysis_sales_part14.png)

#### Part 15: Classifying payment types with hour

![](analysis_sales_part15.png)