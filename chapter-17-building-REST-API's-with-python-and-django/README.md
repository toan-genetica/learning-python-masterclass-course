# Chapter 17: Building REST API's with Python and Django

## Overview

- Introduction
- Building a basic REST API using Django REST Framework
    - Models
    - Serializers
    - Viewsets
    - Creating API Endpoints
    - Adding Image Field
    - Filtering
    - Search functionality
    - API Authentication

### Introduction

- `API` stands for `Application Programming Interface`, which is a software intermediary that allows two applications to talk to each other.
- Most large companies have built APIs for their customers, or for internal use.
- `APIs` allow developers to save time by taking advantage of a platform’s implementation to do the nitty-gritty work

### Building a basic REST API using Django REST Framework

#### Models

- Design a `Task` model.

```
from django.db import models


class Task(models.Model):
    task_name = models.CharField(max_length=200)
    task_desc = models.CharField(max_length=200)
    date_create = models.DateTimeField(auto_now=True)
```

#### Serializers

- `Serializers` allow complex data such as `querysets` and model `instances` to be converted to native Python datatypes that can then be easily rendered into `JSON`, `XML` or other content types.
- `Serializers` provide a `Serializer` class which gives you a powerful, generic way to control the output of your responses, as well as a `ModelSerializer` class which provides a useful shortcut for creating serializers that deal with `model` instances and querysets.

```
from rest_framework import serializers


from .models import Task

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['id', 'task_name', 'task_desc']
```

#### ViewSets

- Django REST framework allows you to combine the logic for a set of related views in a single class, called a `ViewSet`.
- A `ViewSet` class is simply a type of `class-based View`, that does not provide any method handlers such as `.get()` or `.post()`, and instead provides actions such as `.list()` and `.create()`.

```
from rest_framework import viewsets


from .models import Task
from .serializers import TaskSerializer


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all().order_by('-date_create')
    serializer_class = TaskSerializer
```

- Typically, rather than explicitly registering the views in a `viewset` in the `urlconf`, you'll register the `viewset` with a router class, that automatically determines the `urlconf` for you.

#### Routers

- REST framework adds support for `automatic URL routing` to Django, and provides you with a simple, quick and consistent way of wiring your view logic to a set of `URLs`.

```
from django.urls import path
from rest_framework import routers


from .views import TaskViewSet

router = routers.DefaultRouter()
router.register(r'tasks', TaskViewSet)


urlpatterns = router.urls
```

###  Creating API Endpoints

###  Adding Image Field

- `Pillow` is the `Python Imaging Library`. Install `Pillow` by `pip`.

```
# pip install Pillow
```

### Filtering

- Install `django-filter` by `pip`.

```
pip install django-filters
```

- Add code to `tasks/views.py`:

```
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all().order_by('-date_create')
    serializer_class = TaskSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filter_fields = ('completed', )
    ordering = ('-date_create')
```
### Search functionality

- Add code in `views.py`:

```
    ...
    search_fields = ['task_name',]
    ...
```

### API Authentication

- Use `IsAuthenticated` class to build api authentication in example:

```
from rest_framework.permissions import IsAuthenticated
```

#### [CODE REFERENCE](code)