from rest_framework import viewsets
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model

from .models import Task
from .serializers import TaskSerializer, UserSerializer


class TaskViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated,]
    queryset = Task.objects.all().order_by('-date_create')
    serializer_class = TaskSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filter_fields = ('completed', )
    ordering = ('-date_create')
    search_fields = ['task_name',]

class UserCreateView(CreateAPIView):
    model = get_user_model()
    serializer_class = UserSerializer

"""
class DueTaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all().order_by('-date_create').filter(completed=False)
    serializer_class = TaskSerializer


class CompletedTaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all().order_by('-date_create').filter(completed=True)
    serializer_class = TaskSerializer
"""