from django.urls import path, include
from rest_framework import routers


from .views import TaskViewSet, UserCreateView  #, DueTaskViewSet, CompletedTaskViewSet

router = routers.DefaultRouter()
router.register(r'tasks', TaskViewSet)
#router.register(r'due_tasks', DueTaskViewSet)
#router.register(r'completed_tasks', CompletedTaskViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path(r'register/', UserCreateView.as_view())
]