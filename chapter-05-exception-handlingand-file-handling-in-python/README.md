# Chapter 05: Exception Handling & File Handling In Python

## Overview
Python
- Errors & Try Except: Exception Handling
- File Handling

## Manifest File

### Python Try Except
The "try" block lets you test a block of code for errors.
The "except" block lets you handle the error.
The "finally" block lets you execute code, regardless of the result of the try- and except blocks.

#### Exception Handling
When an error occurs, or exception as we call it, Python will normally stop and generate an error message.
These exceptions can be handled using the try statement:
><pre>try:
>   print(x)
>except:
>   print("An exception occurred")</pre>

>The try block will generate an exception, because x is not defined:

#### Many Exceptions
You can define as many exception blocks as you want, e.g. if you want to execute a special block of code for a special kind of error:
><pre>try:
>   print(x)
>except NameError:
>   print("Variable x is not defined")
>except:
>   print("Something else went wrong")

>Print one message if the try block raises a NameError and another for other errors:

#### Else
You can use the else keyword to define a block of code to be executed if no errors were raised:
><pre>try:
>   print("Hello")
>except:
>   print("Something went wrong")
>else:
>   print("Nothing went wrong")<pre>

>The try block does not generate any error:

#### Finally
The finally block, if specified, will be executed regardless if the try block raises an error or not.
><pre>try:
>   print(x)
>except:
>   print("Something went wrong")
>finally:
>   print("The 'try except' is finished")<pre>

This can be useful to close objects and clean up resources:
><pre>try:
>   f = open("demofile.txt")
>   f.write("Lorum Ipsum")
>except:
>   print("Something went wrong when writing to the file")
>finally:
>   f.close()</pre>

### File Handling

#### Reading data from a file
><pre>file = open('demo.txt', 'r')
>print(file.read())
>file.close()</pre>

#### Adding data to a file
><pre>file = open('demo.txt', 'w')
>file.write('This is a first line')
>file.close()</pre>

#### Append data to a file
><pre>file = open('demo.txt', 'a')
>file.write('This is a second line')
>file.close()</pre>