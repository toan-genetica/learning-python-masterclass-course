# Chapter 22: Python Best Practices Writing Clean & Efficient Python Code

## Overview
- Convention code python
    - Style code
    - Namimg

## Manifest File

### Style code

#### Rule 1
- We always make sure that instead of using tabs you use one two three and four spaces.

#### Rule 2
- That is every line of Python code should be 79 characters or less.

#### Rule 3
- The rule for functions and classes actually states that you always need to have two blank lines after each function

```
def add_number(number_one, number_two):
    return number_one + number_two


def sub_number(number_one, number_two):
    return number_one - number_two


class Student:
    pass
```

#### Rule 4
- The next rule which is that the method should be separated by one blank line.

```
class Student:
    
    def display_name(self, name):
        print('name)
```

#### Rule 5
- That is to only include one space before and after variable assignment.

```
mark = 10
```

#### Rule 6
- That is you should not leave spaces around keyword arguments.

```
student_one = Student(name='Toan')
```

### Naming
- So the first rule for naming convention is that classes should always be named in a camel case format.

```
def  print_number(number):
    print(number)
```

- `class` name with singular nouns and first capital letters.

```
class  Student:

    def display_name(self, name):
        print(name)
```