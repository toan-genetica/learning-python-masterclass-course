# Chapter 20: Automation With Selenium Web Driver and Python

## Overview

- Introduction
- Install Selenium
- Opening a URL
- Find elements by XPATH
- Clicking Links
- Refreshing a Webpage
- Using forward and backward navigation buttons
- Scrolling and Getting the Current URL


## Manifest File

### Introduction

- Selenium is an umbrella project for a range of tools and libraries that enable and support the automation of web browsers.
- It provides extensions to emulate user interaction with browsers, a distribution server for scaling browser allocation, and the infrastructure for implementations of the W3C WebDriver specification that lets you write interchangeable code for all major web browsers

### Install Selenium

- Install by pip

```

pip install selenium

```

- Download and extract ChromDriver 84  with Google Chrome v84

### Opening a URL

- WebDriver drives a browser natively, as a user would, either locally or on a remote machine using the Selenium server, marks a leap forward in terms of browser automation.

```

from selenium import webdriver

driver = webdriver.Chrome()
driver.get('http://www.google.com')

```

### Find elements by XPATH

```
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from time import sleep

driver = webdriver.Chrome()
driver.get('http://www.google.com')
ele = driver.find_element_by_xpath('/html/body/div/div/form/div/div/div/div/div/input')
time.sleep(3)
ele.clear()
ele.send_keys("Python")
ele.send_keys(Keys.RETURN)

```

### Clicking Links

```

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from time import sleep

driver = webdriver.Chrome()
driver.get('http://www.google.com')


ele = driver.find_element_by_xpath('//*[@id="gbw"]/div/div/div[1]/div[1]/a')
time.sleep(3)
ele.click()

```

### Refreshing a Webpage

```

time.sleep(12)
driver.refresh()

```

### Using forward and backward navigation buttons

```

time.sleep(5)
driver.back()

time.sleep(5)
driver.forward()

```

### Scrolling and Getting the Current URL

- Scolling page

```
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from time import sleep

driver = webdriver.Chrome()
driver.get('https://www.wikipedia.org')
ele = driver.find_element_by_tag_name('html')
ele.send_keys(Keys.END)
time.sleep(5)
ele.send_keys(Keys.HOME)

```

- Print current url

```
print(driver.current_url)
```