# Chapter 23: Network Programming In Python Using Sockets  Building A Chat Application
## Overview

- IP Address
    - Introduction
    - IP address for a website
- Ports and Sockets
- Creating A Client
- Sending Messages

## Manifest File

### IP Address

#### Introduction
- Internet Protocol address (IP address) is a numerical label assigned to each device connected to a computer network that uses the Internet Protocol for communication

#### IP address for a website
- Websites is located on a server and each server also  has an IP

### Ports and Sockets

#### Ports
- A port is a communication endpoint.
- Ip address is not enough, you also need to specify the port to which you want to connect to.
- Each devies has mutiple ports and each port serves a different purpose.
- To connect some server over a network, you also need its port number.

#### Sockets
- `Socket` is an end point and communication flow between two programs running.
- They allow communication between two different process runnning on the same or different machines.
- Socket Address = IP Address + Port Address
- Acts as a layer of communication between two conputers.

#### Steps involved
- Create a socket for communication.
- Bind the socket with the IP address and the port number of the receiver.
- Send a message via socket

### Creating A Client
- Add code to `client.py`

```
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

HOST_NAME = socket.gethostname()
PORT = 12345
s.connect((HOST_NAME, PORT))
```

- Add code to `server.py`

```
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

HOST_NAME = socket.gethostname()
PORT = 12345
s.bind((HOST_NAME, PORT))
s.listen(4)

while True:
    client, address = s.accept()
    print(address)
```

### Sending Messages

- Add code to client `server.py`
```
while True:
    client, address = s.accept()
    client.send(bytes('Hello client', 'utf-8'))
    print(address)
```

- Add code to `client.py`

```
msg = s.recv(100)
print(msg.decode('utf-8'))
```