# Chapter 06: Some More Types In Python

## Overview
Python
- Dictionaries
- Tuples
- String Functions
- Numeric Functions
## Manifest File

### Dictionaries:
A dictionary is a collection which is unordered, changeable and indexed. In Python dictionaries are written with curly brackets, and they have keys and values.


><pre>thisdict = {
>   "mot": "one",
>   "hai": "two",
>   "ba": "three"
>}</pre>

#### Accessing Items
You can access the items of a dictionary by referring to its key name, inside square brackets:
><pre>x = thisdict["mot"]</pre>

There is also a method called get() that will give you the same result:
><pre>x = x = thisdict.get("mot")</pre>
><pre>x = x = thisdict.get("name", "key not found")</pre>

#### Change Values
You can change the value of a specific item by referring to its key name:

#### Adding Items
Adding an item to the dictionary is done by using a new index key and assigning a value to it:
><pre>thisdict["bon"] = "four"</pre>

#### Removing Items

###### There are several methods to remove items from a dictionary:
><pre>thisdict.pop("bon")</pre>


###### The popitem() method removes the last inserted item (in versions before 3.7, a random item is removed instead):
><pre>thisdict.popitem()</pre>

###### The del keyword removes the item with the specified key name:
><pre>del thisdict["model"]</pre>

###### The del keyword can also delete the dictionary completely:
><pre>del thisdict</pre>

#### Copy a Dictionary

###### Make a copy of a dictionary with the copy() method:
><pre>mydict = thisdict.copy()</pre>

###### Make a copy of a dictionary with the dict() function:
><pre>mydict = dict(thisdict)</pre>

#### Nested Dictionaries
A dictionary can also contain many dictionaries, this is called nested dictionaries.
><pre>myfamily = {
>   "child1" : {
>    "name" : "Emil",
>    "year" : 2004
>  },
>  "child2" : {
>    "name" : "Tobias",
>    "year" : 2007
>  },
>  "child3" : {
>    "name" : "Linus",
>    "year" : 2011
>  }
>}</pre>

### Tuples
A tuple is a collection which is ordered and unchangeable. In Python tuples are written with round brackets.

#### Create a Tuples
><pre>thistuple = ("apple", "banana", "cherry")</pre>

#### Access Tuple Items
You can access tuple items by referring to the index number, inside square brackets:
><pre>thistuple[1]</pre>

#### Change Tuple Values
Once a tuple is created, you cannot change its values. Tuples are unchangeable, or immutable as it also is called.
But there is a workaround. You can convert the tuple into a list, change the list, and convert the list back into a tuple.
><pre>x = ("apple", "banana", "cherry")
>y = list(x)
>y[1] = "kiwi"
>x = tuple(y)</pre>

### Format String
### String Functions
### Numeric Functions