# Chapter 19: Complex Coding Challenges

## Overview

- Challenge 1
- Challenge 2
- Challenge 3
- Challenge 4
- Challenge 5
- Challenge 6
- Challenge 7
- Challenge 8
- Challenge 9


## Manifest File

### Challenge 1

- Build caculator for contructor

###### [Level 1](challenge-1/level1.py)

###### [Level 2](challenge-1/level2.py)

### Challenge 2

- Buid a Tax app

###### [Level 1](challenge-2/tax.py)

###### [Level 2](challenge-2/tax_gui.py)


### Challenge 3

- Build a Text-Editor App.

![](challenge_3.png)

##### [Code reference](challenge-3/text_editor.py)


### Challenge 4

- Build an inventory app

###### [Code reference](challenge-4/inventory.py)

### Challenge 5

- Build a booking app

###### [Code reference](challenge-5/booking.py)

### Challenge 6

- Build a caculator app

###### [Code reference](challenge-6/caculator.py)

### Challenge 7

- Show real time on 5 location

###### [Code reference](challenge-7/ex1.py)

### Challenge 8

- Build an app which displays weather data as per your location using an API.

![](challenge_8.png)

##### [Code reference](challenge-8)

### Challenge 9

- Build a web scrapping with beautifulsoup4

##### [Code reference](challenge-9/scraping.py)