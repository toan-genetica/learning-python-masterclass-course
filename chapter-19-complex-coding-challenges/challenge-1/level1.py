def get_input(val1=None, val2=None):
    val1 = input('Pls type length: ') if val1 is None else val1
    if not val1.isdigit():
        return get_input()
    val2 = input('Pls type breadth: ') if val2 is None else val2
    if not val2.isdigit():
        return get_input(val1=val1)
    return val1, val2


a = get_input()
print(a)
