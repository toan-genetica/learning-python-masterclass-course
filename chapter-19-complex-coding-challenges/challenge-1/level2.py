import tkinter as tk

result = 0
discount = 1
square = 0
total_price = 0

def cal():
    global result, discount, square
    length = int(txt_breadth.get())
    breadth = int(txt_length.get())
    discount = int(txt_discount.get())
    square = length * breadth
    total_price = square * 20
    result = total_price -(total_price * discount /100)
    print()

def print():
    label_square['text'] = 'Square of Floor: '+ str(result)
    label_total_price['text'] = 'Total price: '+ str(result)
    label_result['text'] = 'Total price after discount: '+ str(result)


window = tk.Tk()
window.title('Caculator for contractor')

# Entry 
txt_length = tk.Entry(window)
txt_breadth = tk.Entry(window)
txt_discount = tk.Entry(window)

# Result
label_length = tk.Label(window, text= 'Length Floor: ')
label_breadth = tk.Label(window, text= 'Breadth Floor: ')
label_discount = tk.Label(window, text= 'Discount percent: ')
label_result = tk.Label(window,fg='Red')
label_total_price = tk.Label(window, fg='Red')
label_square = tk.Label(window, fg='Red')

# Grid

# Row 1
label_length.grid(row=1, column=0)
txt_length.grid(row=1, column=1)
# Row 2
label_breadth.grid(row=2, column=0)
txt_breadth.grid(row=2, column=1)
# Row 3
label_discount.grid(row=3, column=0)
txt_discount.grid(row=3, column=1)

btn_cal = tk.Button(window,text='Cal', command=lambda:cal())
btn_cal.grid(row=4)
label_result.grid(row=8)
label_total_price.grid(row=6)
label_square.grid(row=7)

#fr_buttons = tk.Frame(window)


window.mainloop()