import tkinter as tk

result = 0

def tax():
    age = int(txt_age.get())
    annual_income = int(txt_annual_income.get())
    if annual_income in range(0, 20001):
        return 0
    if  annual_income in range(20001, 50001):
        if age < 60:
            return 20
        else:
            return 10
    if  annual_income in range(50001, 100001):
        if age < 60:
            return 30 
        else:
            return 20
    else:
        if age < 60:
            return 40
        else:
            return 30

def cal():
    global result
    annual_income = int(txt_annual_income.get())
    result = annual_income * tax() /100
    print()

def print():
    global re
    label_result['text'] = 'Total Tax: '+ str(result)


window = tk.Tk()
window.title('Caculator for contractor')

# Entry 
txt_age = tk.Entry(window)
txt_annual_income = tk.Entry(window)

# Result
label_age = tk.Label(window, text= 'Your age: ')
label_annual_income = tk.Label(window, text= 'Your annual income : ')

label_result = tk.Label(window,fg='Red')

# Grid

# Row 1
label_age.grid(row=1, column=0)
txt_age.grid(row=1, column=1)
# Row 2
label_annual_income.grid(row=2, column=0)
txt_annual_income.grid(row=2, column=1)

btn_cal = tk.Button(window,text='Cal', command=lambda:cal())
btn_cal.grid(row=4)
label_result.grid(row=6)


#fr_buttons = tk.Frame(window)




window.mainloop()