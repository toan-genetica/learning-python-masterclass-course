import requests, json

base_url1 = "http://worldtimeapi.org/api/timezone/Asia/"
li = ['Qatar', 'Hong_Kong', 'Ho_Chi_Minh', 'Seoul', 'Shanghai']

for i in li:
    res = requests.get(base_url1 + i).json()
    print(res["timezone"])
    print(res["utc_datetime"])