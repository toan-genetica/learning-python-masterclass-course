import psycopg2


def create_table():
    con = psycopg2.connect(
        dbname='vtoanpkdb',
        user='postgres',
        password='T0@n1308',
        host='localhost',
        port='5432'
    )
    cur = con.cursor()
    # insert data to table
    query = '''CREATE TABLE Products(ID SERIAL, PRICE FLOAT, QUANTITY INT);'''
    cur.execute(query)
    print('table is created successfully')
    con.commit()
    con.close()

def add_data():
    con = psycopg2.connect(
        dbname='vtoanpkdb',
        user='postgres',
        password='T0@n1308',
        host='localhost',
        port='5432'
    )
    cur = con.cursor()
    # insert data to table
    id_product = input('Enter ID: ')
    price_product = input('Enter Price: ')
    quantity_product = input('Enter Quantity: ')
    query = '''INSERT INTO Products (ID,PRICE,QUANTITY) VALUES (%s,%s,%s);'''
    cur.execute(query,(id_product, price_product, quantity_product))
    print('data is added successfully')
    con.commit()
    con.close()

def delete_data():
    con = psycopg2.connect(
        dbname='vtoanpkdb',
        user='postgres',
        password='T0@n1308',
        host='localhost',
        port='5432'
    )
    cur = con.cursor()
    # insert data to table
    id_product = input('Enter ID: ')
    query = '''DELETE FROM Products WHERE id = (%s);'''
    cur.execute(query,(id_product))
    print('data is deleted successfully')
    con.commit()
    con.close()

def view_data():
    con = psycopg2.connect(
        dbname='vtoanpkdb',
        user='postgres',
        password='T0@n1308',
        host='localhost',
        port='5432'
    )
    cur = con.cursor()
    query = '''SELECT * FROM products;'''
    cur.execute(query)
    con.commit()
    con.close()