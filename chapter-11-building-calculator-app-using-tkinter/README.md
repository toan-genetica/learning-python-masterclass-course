# Chapter 11: Building Calculator App Using Tkinter

## Overview
Python
- Building a caculator apps by using tkinter module

## Manifest File

### Building Caculator app

#### Import Tkinter and Parser module. Parser is used for caculating the operations
![](caculator.png)

#### Result
![](result.png)

### Reference code
[Caculator app](chapter-11-building-calculator-app-using-tkinter/caculator.py)