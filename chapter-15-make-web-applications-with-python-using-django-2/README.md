# Chapter 15: Make Web Applications In Python Using Django

## Overview
Python
- Using virtual environment
- Django 2.0
    - Creating a project
    - Creating an app
    - Overview of an app
    - Creating a view
    - Migrations
    - Creating a model
    - Creating and Adding database tables
    - Filtering the results
    - Creating a super user
    - Creating another view
    - Connecting to the database
    - Creating templates
    - Using render
    - Raising 404 Error
    - Designing the detail view
    - Removing hard coded URLs
    - Using Namespaces
    - Designing Navbar
    - Navbar Touchup

## Manifest File

### Creating our first Django project
- Creating a virtualenv 
```
# python -m venv env
```
- Activating a virtualenv
```
# source env/bin/activate
```
- Creating a Django project
```
(env)# pip install django==2.0
(env)# django-admin.py startproject config .
```

### Creating an app
- Creating an app in Django project
```
(env)# python3 manage.py startapp photo_app
```

### Overview of an app.
- The `migrations` folder is where Django stores `migrations`, or changes to your database. There’s nothing in here you need to worry about.
    - `__init__.py` tells Python that your pages app is a package.
    - `admin.py` is where you register your models with the Django admin application.
    - `apps.py` is a configuration file common to all Django apps.
    - `models.py` is where the models for your app are located.
    - `tests.py` contains test procedures that will be run when testing your app.
    - `views.py` is where the views for your app are located.


### Creating a view
- A minor difference with version 1.11 in `urls.py`:
```
from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse('<h1>This is Homepage</h1>')
```

#### Migrations
```
# python manage.py migrate
```

### Creating a model
```
class Photo(models.Model):
    name = models.CharField(max_length=100)
    creator = models.CharField(max_length=100)
    price = models.CharField(max_length=100)
```


#### Creating and Adding database tables
- Config in `INSTALLED_APPS` in `settings.py` file
```
INSTALLED_APPS = [
    'photo_app.apps.PhotoAppConfig',
    ...
]
```
```
# python manage.py makemigrations photo_app
# python manage.py sqlmigrate photo_app 0001
# python manage.py migrate
```
```
# python manage.py shell

>>> from photo_app.models import Photo
>>> obj = Photo()
>>> obj.name = 'Photo1'
>>> obj.creator = 'Toan'
>>> obj.price = '222'
>>> obj.save()
```

### Filtering the results
- Filtering all photos in database
```
Photo.objects.all()
```

- Filtering photos by `id`
```
Photo.objects.filter(id=1)
```

### Creating a super user
```
# python manage.pu createsuperuser
```

### Creating another view
- Add the line code to `photo_app/urls.py`
```
#book/urls.py
...
path('<int:photo_id>/',  detail, name='detail'),
...

```
- Add code in `photo_app/views.py`
```
def detail(request, photo_id):
    return  HttpResponse("<h1>Detail page: "+ str(photo_id)+ "</h1>")
```

### Connecting to the database
- Import `Photo` from `photo_app.models` to get data from `Photo` table.
```
from .models import Photo
```
- Modify `index()` function to get data.
```
def index(request):
    html = ''
    obj_list = Photo.objects.all()
    for photo in obj_list:
        url = '/photos/' + str(photo.id) + '/'
        html += '<a href="'+url+'">' + str(photo.name) + '</a><br>'
    return HttpResponse(html)
```

### Creating templates
- Create a `templates` folder in `photo_app` directory, and create a subfolder of `templates` is `photo`, then add `index.html` file into it.
- Import `loader` from `django.template`.
```
from django.template import loader
```
- Modify `index()` function.
```
def index(request):
    template = loader.get_template('photo/index.html')
    object_list = Photo.objects.all()
    context = {
        'object_list': object_list,
    }
    return HttpResponse(template.render(context, request))
```

### Using render
- Import `render` from `django.shortcuts`.
```
from django.shortcuts import render
```
- Modify `index()` function.
```
def index(request):
    object_list = Photo.objects.all()
    context = {
        'object_list': object_list,
    }
    return render(request, 'photo/index.html', context)
```

### Raising 404 Error
- Import `Http404` from `django.http`.
```
from django.http import HttpResponse, Http404
```
- Modify `detail()` function.
```
def detail(request, photo_id):
    try:
        photo = Photo.objects.get(id=photo_id)
    except Photo.DoesNotExist:
        raise Http404('Photo not found')
    return render(request, 'photo/detail.html', {'photo': photo})
```

### Designing the detail view
```
    {{ photo.name }}
    {{ photo.creator }}
    {{ photo.price }}
```
- Add code to retrieve infomations of photo.

### Removing hard coded URLs
```
<a href='{% url 'detail' object.id %}'>{{ object.name }}</a>
```

### Using Namespaces
- Add the line code to `photo_app/urls.py`
```
app_name = 'photos'
```
- Modify `href` of link in `index.html`
```
href='{% url 'photos:detail' object.id %}
```

### Designing Navbar
### Navbar Touchup