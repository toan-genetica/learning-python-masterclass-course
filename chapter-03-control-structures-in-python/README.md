# Chapter 03: Control Structures In Python

## Overview
Python - Conditions, Lists, Functions, For Loops, While Loops, Boolean logic in Python

## Manifest File

### Python Conditions and If, Elif, Else statements:
Python supports the usual logical conditions from mathematics:

    Equals: a == b
    Not Equals: a != b
    Less than: a < b
    Less than or equal to: a <= b
    Greater than: a > b
    Greater than or equal to: a >= b

### Python Lists
A list is a collection which is ordered and changeable. In Python lists are written with square brackets.

<pre>>>>thislist = ["apple", "banana", "cherry"]</pre>

##### Access Items
You access the list items by referring to the index number:
<pre>>>>thislist[0] = "apple"</pre>

##### Negative Indexing
Negative indexing means beginning from the end, -1 refers to the last item, -2 refers to the second last item etc.
<pre>>>>thislist[-1] = "banana"</pre>

### FUNCTIONS
function is a block of code which only runs when it is called.
You can pass data, known as parameters, into a function.
A function can return data as a result.

##### Creating a Function
In Python a function is defined using the "def" keyword:
<pre>
>>>def first_functions():
>>>    print('Hello World!')
</pre>

##### Calling a Function
To call a function, use the function name followed by parenthesis:
<pre>>>>first_functions()</pre>

### FOR LOOPS
A for loop is used for iterating over a sequence (that is either a list, a tuple, a dictionary, a set, or a string).
<pre>
>>>fruits = ["apple", "banana", "cherry"]
>>>for x in fruits:
>>>  print(x)
</pre>

### While Loops
With the while loop we can execute a set of statements as long as a condition is true.
Print i as long as i is less than 6:
<pre>
>>>i = 1
>>>while i < 6:
>>>  print(i)
</pre>

### Boolean logic in Python